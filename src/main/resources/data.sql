INSERT INTO `promotion` (`promotion_id`,`user_id`,`store_id`,`promotion_code`,`promotion_status`, `promotion_type`, `promotion_name`, `expiry_date`, `date_created`)
VALUES(1,1,1,'user1',1,'DISCOUNT','% 10 INDIRIM','10.01.2019','10.01.2018'),
      (2,2,1,'user2',1,'GIFT','2 PAKET FOSFOR','05.02.2018','10.01.2018'),
      (3,2,1,'user2',0,'GIFT','1 PAKET CANLI MAMUN','05.09.2018','10.01.2018'),
      (4,1,1,'user1',0,'DISCOUNT','% 15 INDIRIM','05.03.2020','10.01.2018');


INSERT INTO `store` (`store_id`, `store_address`, `store_name`, `store_type`)
VALUES (1,'CANAKKALE','GUNDUZ BALIKCILIK','BALIK AV MALZEMELERI VE YEM'),
       (2,'CANAKKALE','BOZ BALIKCILIK','BALIK AV MALZEMELERI'),
       (3,'CANAKKALE','DENIZ BALIKCILIK','BALIK AV MALZEMELERI');

INSERT INTO `role` (`role_id`, `role`)
VALUES(1,'ADMIN'),
      (2,'USER'),
      (3,'STORE');

INSERT INTO `user` (`user_id`, `active`, `email`, `last_name`, `name`, `password`)
VALUES
	(1,1,'admin@gmail.com','GUNDUZ','Suleyman','suleyman'),
	(2,1,'admin@gmail.com','CAGLAYAN','Hakan','hakan'),
	(3,1,'tugce@gmail.com','VATANSEVER','Tugce','tugce');

INSERT INTO `user_role` (`user_id`, `role_id`)
VALUES(1,1),
      (3,2);