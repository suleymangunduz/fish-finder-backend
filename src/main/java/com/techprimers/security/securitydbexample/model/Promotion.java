package com.techprimers.security.securitydbexample.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "promotion")
public class Promotion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "promotion_id", nullable = false)
    private Integer promotionId;

    @Column(name = "promotion_code", nullable = false)
    private String promotionCode;

    @Column(name = "user_id", nullable = false)
    private Integer userId;

    @Column(name = "store_id", nullable = false)
    private Integer storeId;

    @Column(name = "promotion_status", nullable = false)
    private int promotionStatus;

    @Column(name = "promotion_type", nullable = false)
    private String promotionType;

    @Column(name = "promotion_name", nullable = false)
    private String promotionName;

    @Column(name = "expiry_date", nullable = false)
    private String expiryDate;

    @Column(name = "date_created", nullable = false)
    private String dateCreated;

    public Promotion() {
    }

    public Promotion(Integer promotionId, String promotionCode, Integer userId, Integer storeId, int promotionStatus, String promotionType, String promotionName, String expiryDate, String dateCreated) {
        this.promotionId = promotionId;
        this.promotionCode = promotionCode;
        this.userId = userId;
        this.storeId = storeId;
        this.promotionStatus = promotionStatus;
        this.promotionType = promotionType;
        this.promotionName = promotionName;
        this.expiryDate = expiryDate;
        this.dateCreated = dateCreated;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    public int getPromotionStatus() {
        return promotionStatus;
    }

    public void setPromotionStatus(int promotionStatus) {
        this.promotionStatus = promotionStatus;
    }

    public String getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public String  getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String  dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }
}
