package com.techprimers.security.securitydbexample.advice.exception_model;

public class PromotionServiceException extends Error {
    private static final long serialVersionUID = -470180507998010368L;

    public PromotionServiceException() {
        super();
    }

    public PromotionServiceException(final String message) {
        super(message);
    }
}
