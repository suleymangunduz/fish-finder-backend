package com.techprimers.security.securitydbexample.advice.exception_model;

public class StoreServiceException extends Exception {
    private static final long serialVersionUID = -470180507998010368L;

    public StoreServiceException() {
        super();
    }

    public StoreServiceException(final String message) {
        super(message);
    }
}
