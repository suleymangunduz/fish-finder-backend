package com.techprimers.security.securitydbexample.service;

import com.techprimers.security.securitydbexample.model.Role;
import com.techprimers.security.securitydbexample.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public List<Role> getAllRoles() {
        return roleRepository.findAll();
    }

    public Role getRoleByRoleId(Integer roleId) {
        return roleRepository.findByRoleId(roleId);
    }

    public void deleteAllRoles() {
        roleRepository.deleteAll();
    }

    public void deleteRoleByRoleId(Integer roleId) {
        roleRepository.deleteByRoleId(roleId);
    }
}
