package com.techprimers.security.securitydbexample.service;


import com.techprimers.security.securitydbexample.advice.exception_model.PromotionServiceException;
import com.techprimers.security.securitydbexample.controller.PromotionController;
import com.techprimers.security.securitydbexample.model.Promotion;
import com.techprimers.security.securitydbexample.repository.PromotionRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class PromotionService {

    final static Logger logger = Logger.getLogger(PromotionController.class);

    @Autowired
    private PromotionRepository promotionRepository;

    public void addPromotion(Promotion promotion) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        try {
            if(promotionRepository.exists(promotion.getPromotionId())) {
                logger.error("Promosyon ID = " + promotion.getPromotionId() + " is already created, should be unique !");
            } else {
                promotion.setDateCreated(dateFormat.format(date));
                promotionRepository.save(promotion);
            }
        } catch(PromotionServiceException exception) {
            throw new PromotionServiceException("Internal Server Exception while creating promotion !");
        }
    }

    public List<Promotion> getPromotionsByStoreId(Integer storeId) throws PromotionServiceException {
        return promotionRepository.findAllByStoreIdEquals(storeId);
    }

    public List<Promotion> getPromotionsByUserId(Integer userId) throws PromotionServiceException {
        return promotionRepository.findAllByUserIdEquals(userId);
    }

    public List<Promotion> getAllPromotions() throws PromotionServiceException {
        return promotionRepository.findAll();
    }

    public List<Promotion> getPromotionsByStatus(int statusCode) throws PromotionServiceException {
        return promotionRepository.findAllByPromotionStatusEquals(statusCode);
    }

    public List<Promotion> getPromotionsByType(String promotionType) throws PromotionServiceException {
        return promotionRepository.findAllByPromotionTypeEquals(promotionType);
    }

    public boolean isExistById(Integer promotionId) throws PromotionServiceException {
        return promotionRepository.exists(promotionId);
    }

    public static Promotion createPromotion(Integer userId, Integer storeId) {

        Promotion promotion = new Promotion();
        return promotion;
    }
}
