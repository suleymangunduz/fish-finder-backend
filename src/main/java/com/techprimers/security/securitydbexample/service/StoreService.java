package com.techprimers.security.securitydbexample.service;

import com.techprimers.security.securitydbexample.model.Promotion;
import com.techprimers.security.securitydbexample.model.Store;
import com.techprimers.security.securitydbexample.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoreService {

    @Autowired
    private StoreRepository storeRepository;


    public void saveStore(Store store) {
        storeRepository.save(store);
    }

    public List<Store> getAllStores() {
        return storeRepository.findAll();
    }

    public Store getStoreById(Integer storeId) {
        return storeRepository.findStoreByStoreId(storeId);
    }

    public List<Store> getStoreByAddress(String storeAddress) {
        return storeRepository.findStoreByStoreAddress(storeAddress);
    }

    public List<Store> getStoreByCity(String city) {
        return storeRepository.findStoreByStoreAddressContains(city);
    }

    public List<Store> getStoreByStoreName(String storeName) {
        return storeRepository.findStoreByStoreNameContains(storeName);
    }

    public List<Store> getStoreByStoreType(String storeType) {
        return storeRepository.findStoreByStoreTypeContains(storeType);
    }

    public void deleteStoreById(Integer storeId) {
        storeRepository.deleteStoreByStoreId(storeId);
    }

    public void deleteStoreByStoreName(String storeName) {
        storeRepository.deleteStoreByStoreNameContains(storeName);
    }

    public boolean isExistByName(String storeName) {
        return storeRepository.findStoreByStoreNameEquals(storeName);
    }

}
