package com.techprimers.security.securitydbexample.controller;

import com.techprimers.security.securitydbexample.advice.ResourceNotFoundException;
import com.techprimers.security.securitydbexample.advice.exception_model.PromotionServiceException;
import com.techprimers.security.securitydbexample.model.Promotion;
import com.techprimers.security.securitydbexample.service.CustomUserDetailsService;
import com.techprimers.security.securitydbexample.service.PromotionService;
import com.techprimers.security.securitydbexample.service.StoreService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/promotions")
@RestController
public class PromotionController {

    //exception handling ve loglama eklenecek !.
    //getMapping ler degistirilecek !.

    //@Slf4j notasyonu ve dependency ekle loglama icin.

    //bu exception olayları servis katmanına alıcaz.

    //relation ları composite yaparsan eger jpa senin icin istedigin ara tabloyu oluşturuyor olucak zaten

    // mesela user ın promosyonlarını tutmak için user sınıfı ıcerısınde onetomany olarak promosyonlar alan bir liste tanımlaman lazım


    @Autowired
    private PromotionService promotionService;

    /* En fazla ugrastiracak yerlerden birisi burasi, Kontrolleri duzgun yapmak lazim !.*/

    @RequestMapping(value = "/addPromotion", method = RequestMethod.POST)
    public void addPromotion(@RequestBody Promotion promotion) {
        promotionService.addPromotion(promotion);
    }

    @RequestMapping(value = "/getByStoreId", method = RequestMethod.POST)
    public List<Promotion> getPromotionsByStoreId(@RequestParam Integer storeId) {
        return promotionService.getPromotionsByStoreId(storeId);
    }

    @RequestMapping(value = "/getByUserId", method = RequestMethod.POST)
    public List<Promotion> getPromotionsByUserId(@RequestParam Integer userId) {
        return promotionService.getPromotionsByUserId(userId);
    }

    @GetMapping("/all")
    public List<Promotion> getAllPromotions() throws ResourceNotFoundException, PromotionServiceException {
        try {
            if(promotionService.getAllPromotions() == null) {
                throw new ResourceNotFoundException("No promotion found!");
            }
            return promotionService.getAllPromotions();

        } catch(PromotionServiceException exception) {
            throw new PromotionServiceException("Internal Server Exception while getting promotions!");
        }
    }

    @GetMapping("/getActivePromotions")
    public List<Promotion> getActivePromotions() throws ResourceNotFoundException, PromotionServiceException {
        try {
            if(promotionService.getPromotionsByStatus(1) == null) {
                throw new ResourceNotFoundException("No active promotions !");
            }
            return promotionService.getPromotionsByStatus(1);
        } catch(PromotionServiceException exception) {
            throw new PromotionServiceException("Internal Server Exception while getting promotions!");
        }
    }

    @GetMapping("/getInactivePromotions")
    public List<Promotion> getInactivePromotions() {
        return promotionService.getPromotionsByStatus(0);
    }

    @GetMapping("/getDiscountPromotions")
    public List<Promotion> getDiscountPromotions() {
        return promotionService.getPromotionsByType("DISCOUNT");
    }

    @GetMapping("/getGiftPromotions")
    public List<Promotion> getGiftPromotions() {
        return promotionService.getPromotionsByType("GIFT");
    }

}
