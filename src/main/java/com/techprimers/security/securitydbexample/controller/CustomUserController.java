package com.techprimers.security.securitydbexample.controller;

import com.techprimers.security.securitydbexample.model.Promotion;
import com.techprimers.security.securitydbexample.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/users")
@RestController
public class CustomUserController {

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    /*@RequestMapping(value = "/getPromotions", method = RequestMethod.POST)
    public List<Promotion> getPromotions(@RequestParam Integer userId) {
        return customUserDetailsService.getPromotions(userId);
    }*/
}
