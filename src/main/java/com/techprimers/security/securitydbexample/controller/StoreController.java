package com.techprimers.security.securitydbexample.controller;

import com.techprimers.security.securitydbexample.model.Promotion;
import com.techprimers.security.securitydbexample.model.Store;
import com.techprimers.security.securitydbexample.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/store")
@RestController
public class StoreController {

    /* Exception handling ve loglar eklenecek
       Ayrıca save methodunda uniqe ekleme yapılması için gerekli kontrolü eklemek gerek.
     */

    @Autowired
    private StoreService storeService;

    @GetMapping("/all")
    public List<Store> getAllStores() {
        return storeService.getAllStores();
    }

    @RequestMapping(value = "/addStore", method = RequestMethod.POST, headers = "Accept=application/json")
    public void addStore(@RequestBody Store store) {
        if(storeService.isExistByName(store.getStoreName())) {
            System.out.print("Store Already Created.!!!");
        } else {
            storeService.saveStore(store);
        }
    }

    /*@RequestMapping(value = "/getPromotions", method = RequestMethod.GET)
    public List<Promotion> getPromotions(@RequestParam Integer storeId) {
        return storeService.getPromotions(storeId);
    }*/

    @RequestMapping(value = "/getById", method = RequestMethod.POST)
    public Store getStoreById(@RequestParam Integer storeId) {
        return storeService.getStoreById(storeId);
    }

    @RequestMapping(value = "/getByAddress", method = RequestMethod.GET)
    public List<Store> getStoreByAddress(@RequestParam String storeAddress) {
        return storeService.getStoreByAddress(storeAddress);
    }

    @RequestMapping(value = "/getByCity", method = RequestMethod.GET)
    public List<Store> getStoreByCity(@RequestParam String storeCity) {
        return storeService.getStoreByCity(storeCity);
    }

    @RequestMapping(value = "/getByName", method = RequestMethod.GET)
    public List<Store> getStoreByName(@RequestParam String storeName) {
        return storeService.getStoreByStoreName(storeName);
    }

    @RequestMapping(value = "/getByType", method = RequestMethod.GET)
    public List<Store> getStoreByType(@RequestParam String storeType) {
        return storeService.getStoreByStoreType(storeType);
    }

    @RequestMapping(value = "/deleteById", method = RequestMethod.GET)
    public void deleteStoreById(@RequestParam Integer storeId) {
        storeService.deleteStoreById(storeId);
    }

    @RequestMapping(value = "deleteByName", method = RequestMethod.GET)
    public void deleteStoreByName(@RequestBody String storeName) {
        storeService.deleteStoreByStoreName(storeName);
    }

}
