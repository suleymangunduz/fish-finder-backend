package com.techprimers.security.securitydbexample.repository;

import com.techprimers.security.securitydbexample.model.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PromotionRepository extends JpaRepository <Promotion, Integer> {

    List<Promotion> findAll();
    List<Promotion> findAllByPromotionStatusEquals(int statusCode);
    List<Promotion> findAllByPromotionTypeEquals(String promotionType);
    List<Promotion> findAllByStoreIdEquals(Integer storeId);
    List<Promotion> findAllByUserIdEquals(Integer userId);
}