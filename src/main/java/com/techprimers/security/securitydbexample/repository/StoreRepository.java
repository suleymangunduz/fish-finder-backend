package com.techprimers.security.securitydbexample.repository;

import com.techprimers.security.securitydbexample.model.Store;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StoreRepository extends JpaRepository <Store, Integer> {

    List<Store> findAll();
    Store findStoreByStoreId(Integer storeId);
    List<Store> findStoreByStoreAddress(String storeAddress);
    List<Store> findStoreByStoreAddressContains(String city);
    List<Store> findStoreByStoreNameContains(String storeName);
    List<Store> findStoreByStoreTypeContains(String storeType);
    boolean findStoreByStoreNameEquals(String storeName);
    void deleteStoreByStoreId(Integer storeId);
    void deleteStoreByStoreNameContains(String storeName);
}
