package com.techprimers.security.securitydbexample.repository;

import com.techprimers.security.securitydbexample.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleRepository extends JpaRepository <Role, Integer> {

    //Yeni rol eklemek icin sorgu yazilmasi gerek, bir caresine bakarsın artık.

    List<Role> findAll();
    Role findByRoleId(Integer roleId);
    void deleteByRoleId(Integer roleId);
    void deleteAll();
}
